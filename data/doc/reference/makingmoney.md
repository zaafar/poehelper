# Making money

Sample reference doc - making money ideas, 
mostly from a recent Reddit thread.


## Basic

- buy/farm Shaper/Elder item sets, get 2 Exa shards


## Syndicate

++It That Fled @ Research: 
    upgrade breachstones (~80-100c x 2)
+Vorici @ Research: white sockets
    lvl1: [The Pariah] (~40c)
    lvl3: 6L item e.g. [Queen of the Forest]
Cameria:
    @ Intervention: Sulphite Scarabs (~30c x 2)
    @ Fortification: Harbinger Orbs
Rin @ Intervention: Cartography Scarabs
Korell @ Research: Fossils (~20c)    


## Merged items

- [Invictus Solaris] + [Vix Lunaris] = [Magna Eclipsis]
- [First Piece of Storms] + [Second Piece of Storms] + [Third Piece of Storms] = [The Tempest's Binding]
- [Agnerod North] + East + West + South = [The Vinktar Square]
- [Berek's Grip] + [Berek's Pass] + [Berek's Respite] = [The Taming]


## Instant "crafting"

- good armors + [The Jeweller's Touch] = 5L


## Fated uniques

- [The Magnate] + {prophecy} [The Great Mind of the North] = [The Tactician]


## Breach blessing upgrades

- [Blessing of Chayula] + one of:
    - [The Blue Dream] = [The Blue Nightmare]
    - [Skin of the Loyal] = [Skin of the Lords]
    - [The Red Dream] = [The Red Nightmare]
    - [Severed in Sleep] = [United in Dream]
    - [Eye of Chayula] = [Presence of Chayula]
    - [The Green Dream] = [The Green Nightmare]
- [Blessing of Uul-Netol] + one of:
    - [The Infinite Pursuit] = [The Red Trail]
    - [The Anticipation] = [The Surrender]
    - [Uul-Netol's Kiss] = [Uul-Netol's Embrace]
- [Blessing of Xoph] + one of:
    - [The Formless Flame] = [The Formless Inferno]
    - [Xoph's Inception] = [Xoph's Nurture]
    - [Xoph's Heart] = [Xoph's Blood]
- [Blessing of Esh] + one of:
    - [Esh's Mirror] = [Esh's Visage]
    - [Voice of the Storm] = [Choir of the Storm]
    - [Hand of Thought and Motion] = [Hand of Wisdom and Action]
- [Blessing of Tul] + one of:
    - [Tulborn] = [Tulfall]
    - [The Snowblind Grace] = [The Perfect Form]
    - [The Halcyon] = [The Pandemonius]

