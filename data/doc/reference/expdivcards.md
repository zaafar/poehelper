# Expensive cards


## Expensive cards

- [House of Mirrors] => Mirror of Kalandra @ [The Alluring Abyss], [The Eternal Labyrinth], [Diviner's Strongbox]
- [The Doctor] => [Headhunter] @ [Burial Chambers], [Spider Forest]
- [Beauty Through Death] => {prophecy} [The Queen's Sacrifice] @ [Vaal Temple]
- [The Fiend] => Corr. [Headhunter] @ [Shrine], [The Putrid Cloister]
- [The Immortal] => {divcard} [House of Mirrors] @ [The Hall of Grandmasters]
- [The Nurse] => {divcard} [The Doctor] @ [Shavronne's Tower], [Tower]
- [Immortal Resolve] => {prophecy} [Fated Connections] @ [Carcass], [Vault]
- [The Samurai's Eye] => [Watcher's Eye] @ [Abyssal Lich] in [Abyssal Depths]
- [Abandoned Wealth] => 3 Exalted Orbs @ [Arsenal], [Mao Kun], [Precinct]
- [The Queen] => [Atziri's Acuity] @ [Vaults of Atziri]
