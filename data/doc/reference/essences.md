# Essences

## Drop levels

L1: Whispering: 1+
L2: Muttering: 12+
L3: Weeping: 30+
L4: Wailing: 48+
L5: Screaming: 68+
L6: Shrieking: Upgrade or corrupt L5; can reforge
L7: Deafening: Upgrade L6; can reforge

**Hysteria / Insanity / Horror / Delirium** can be
obtained by corrupting *Dread, Envy, Misery* or *Scorn*


## Essence of Anger

- weapons: add #fire damage (attacks)
- armor, belt: %fire res
- ring, amulet: %fire damage


## Essence of Anguish

- weapons: add #fire damage to spells
- glove, quiver, ring, amulet: add #fire damage (attacks)
- body, : avoid fire damage
- boots, belt: avoid ignite


## todo...
