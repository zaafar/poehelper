# Incursion

## Armourer's Workshop

T2: Armoury
T3: Chamber of Iron
-+10/20/30% monster life
chests full of armour

## Corruption Chamber

T2: Catalyst of Corruption
T3: Locus of Corruption
--10% max. player resistances
+Altar of Corruption @ T3

## Explosives Room

T2: Demolition Lab
T3: Shrine of Unmaking
+1/2/3 explosive charges

## Flame Workshop

T2: Omnitect Forge
T3: Crucible of Flame
-Omnitect + damage
+fire item @ T3

## Gemcutter's Workshop

T2: Department of Thaumaturgy
T3: Doryani's Institute
+chests full of gems; corrupt @ T3

## Guardhouse

T2: Barracks
T3: Hall of War
!+10/20/30% Monster pack size
+inc. monster pack size

## Hatchery

T2: Automaton Lab
T3: Hybridisation Chamber
-Omnitect + minions
+minion item @ T3

## Jeweller's Workshop

T2: Jewellery Forge
T3: Glittering Halls
+chests full of jewellery

## Lightning Workshop

T2: Omnitect Reactor Plant
T3: Conduit of Lightning
-Omnitect + lightning damage
+lightning item @ T3

## Poison Garden

T2: Cultivar Chamber
T3: Toxic Grove
-caustic plants
+poison item @ T3

## Pools of Restoration

T2: Sanctum of Vitality
T3: Sanctum of Immortality
-4/6/8% monster life regen
+life? item @ T3

## Royal Meeting Room

T2: Hall of Lords
T3: Throne of Atziri
-+10/15/20% monster att./cast speed
+Queen Atziri @ T3

## Sacrificial Chamber

T2: Hall of Offerings
T3: Apex of Ascension
+sacrifice a unique item

## Shrine of Empowerment

T2: Sanctum of Unity
T3: Temple Nexus
-+10/15/20% monster att./cast speed
++upgrades connected rooms by one tier @ T3

## Sparring Room

T2: Arena of Valour
T3: Hall of Champions
-+10/15/20% monster damage
chests full of weapons

## Splinter Research Lab

T2: Breach Containment Chamber
T3: House of the Others
+breach splinters/1 breach/3 breaches

## Storage Room

T2: Warehouses
T3: Museum of Artifacts
+chests full of items

## Strongbox Chamber

T2: Hall of Locks
T3: Court of Sealed Death
strongboxes

## Surveyor's Study

T2: Office of Cartography
T3: Atlas of Worlds
++chests full of maps

## Tempest Generator

T2: Hurricane Engine
T3: Storm of Corruption
+corrupting or radiating tempests @ T3, item

## Torment Cells

T2: Torture Cages
T3: Sadist's Den
tormented spirits

## Trap Workshop

T2: Temple Defense Workshop
T3: Defense Research Lab
-traps throughout the temple
+trap item @ T3

## Vault

T2: Treasury
T3: Wealth of the Vaal
+chests full of currency

## Workshop

T2: Engineering Department
T3: Factory
-+15/25/35% boss life
+5/10/15% IIQ


## Reference

- https://pathofexile.gamepedia.com/List_of_incursion_rooms
