"""
PoEHelper trade plugin.

Author: Vlad Topan (vtopan/gmail)
"""

from iopoelib.misc import parse_wtb_pm

from poehelper.poeh_plugins import PoehPlugin



class TradePlugin(PoehPlugin):
    CFG = {
        }


    def pm_handler(self, message, sender, timestamp=None, **kwargs):
        """
        Handle trade-related private messages received.
        """
        msg = parse_wtb_pm(message, sender=sender)
        if msg:
            self.api.new_event('wtb_pm', msg, timestamp=timestamp)
            
            
    