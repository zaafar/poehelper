"""
PoEHelper macros plugin.

Author: Vlad Topan (vtopan/gmail)
"""

from poehelper.poeh_plugins import PoehPlugin


class MacroPlugin(PoehPlugin):
    CFG = {
        'macros': {
            'F2': '{cr}/oos{cr}',
            'F3': '{cr}/hideout{cr}',
            'F4': '{cr}/kick $player{cr}',
            'F5': '{ctrl+enter}thanks!{cr}',
            },
        }


    def init(self):
        """
        Initialize the plugin.
        """
        if self.cfg['macros'].get('F4') == "{ctrl+enter}thanks!{cr}{cr}/kick $player{cr}":
            self.api.log('Saying thanks and leaving party on F4 appears to be disallowed by the ToS, splitting in F5+F4')
            self.cfg['macros']['F4'] = '{cr}/kick $player{cr}'
            self.cfg['macros']['F5'] = '{ctrl+enter}thanks!{cr}'
        shown_error = 0
        for k, text in self.cfg['macros'].items():
            if '$player' in text:
                if not self.cfg['game']['playername']:
                    if not shown_error:
                        self.api.err('Set game.playername in config.json to your player name!')
                        shown_error = 1
                    continue
                text = text.replace('$player', self.cfg['game']['playername'])
            self.api.add_game_hotkey(k, self.api.send_macro_callback(text))

