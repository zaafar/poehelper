PoE Helper
==========

![PoEHelper main window](screenshots/main.png)

Plugin-able Path of Exile game overlay with useful information and text macros.

**Important note**: To the best of my knowledge and effort this overlay doesn't break any of GGG's
ToS:

- the only effect on the game is sending text macros (by default `/oos` on `F2`, `/hideout` on `F3`,
    `"thanks!"` on `F5` and `kick self` on `F4`), and this behavior is fine in several other tools
- the game memory *is not read* in any way - the only sources of "live" information are the game's
    `client.txt` log and on-demand screenshots, which are currently not used
- no "unfair advantage" information is shown; nothing about the live state of the game is monitored
    (like flasks or buffs)


**Important note #2**: Really, *really* alpha version; it WILL have bugs - see below.


Why
---

After the Nth "reference wallpaper" popped up on reddit, I decided to write a simple overlay to
display the relevant information when needed, without having to leave the game to zoom in on a huge
PNG in some other program to find out whether I wanted to kill or kick Riker in/from Fortification,
and then check Guff, and then check Riker again because the process took long enough to forget the
first one. I had also wanted for some time to replace the N tools that do basic things (or rather
from which I use simple features) with a single one. One thing led to another and a full,
plugin-able overlay UI was born.

Easily searchable reference about other things (e.g. fossils) seemed like an obvious thing to add,
text macros, and so on. `Ctrl+C` on items to get the current price I had partly done as sample code
a few weeks back, so that went in as well.

Notes:

- **This is an alpha version** - the UI is still rough and may break at all seams, the
    the plugin API is less coherent/complete than it could be, etc. The point of this release is to
    gauge public interest in this kind of a tool, which in theory could replace a bunch of other
    programs in time and make life easier. The initial feedback was great, thank you!
- I just found out something similar exists (see
    https://www.reddit.com/r/pathofexile/comments/9ifzko/release_poe_addon_launcher_and_manager_pal/
    ); I don't plan to integrate other tools as a whole, so until / unless whatever tool shows up as
    a plugin in PoEHelper, something like that is probably what you want. If this gets enough
    interest though, most things should be easy enough to reimplement.


Features
--------

- easily minimize-able (`Alt-F2`); minimizes itself on entering non-hideout/town areas by default
    - position of minimized window tracked separately, so it can be moved out of the way
- easily-accessible and searchable reference for **syndicate**, **fossils**, expensive **divination
    cards**, **maps**, **gems**, etc.
    - easy to add custom reference information - simply create a markdown-like file under
        `data/doc/reference` (second-level titles - those starting with `##` - are treated as
        sections, titles between square brackets are converted to wiki links, etc.)
    - jump to the reference search box by pressing `F`
    - `Alt+E` to try to figure out which syndicate members are on visible on the map and expand
        their reference entries
- keyboard shortcuts to send macros (default: `F2` to `oos`, `F3` to go to hideout, `F4` to say
    thanks & leave party - this needs your usename set in `config.json`)
- on in-game `Ctrl+C` on an item:
    - for *maps*, *currency*, *scarabs* and *uniques*, query the official trading site for the
        lowest 20 prices
    - if the item has DPS, life or resists, compute them (phys/ele/chaos/total)
- "Price Watch": on-click querying *poe.ninja* and the official trade site for the current prices of
    user-configured items
- area monitor: log each non-town/HO area and compute min/max/avg of time spent (useful for areas
    before maps, or for maps if you only enter the map once - no way to tell otherwise if it's the
    same map)
- easily reachable timer (click to start, click to reset) and counter (click to increment,
    right-click to reset to 0)
- **very easy to extend via plugins**:
    - events are generated on e.g. area changes and `Ctrl+C` in-game with pre-parsed information
    - each plugin designs its UI panel via a very simple textual description, and the displayed
        fields are easy to update
    - API to add custom keyboard shortcuts (which are only triggered while the game is active)
- configurable
    - see `data/config.json` and `data/config_plugins.json`
    - the UI can be customized via `data/poehelper.css`


Screenshots
-----------

- reference windows:
    - syndicate:
        ![Syndicate reference](screenshots/reference-syndicate.png)
    - fossils:
        ![Fossil reference](screenshots/reference-fossils.png)
    - expensive divination cards:
        ![Expensive divination card reference](screenshots/reference-divcards.png)
    - maps (by tag: maps with 3 bosses and T1 divination cards):
        ![Map reference](screenshots/reference-maps.png)
    - gems (by tag: curse / intelligence / spell):
        ![Gem reference](screenshots/reference-gems.png)
- the price watch plugin:
    ![PriceWatch plugin](screenshots/plugin-pricewatch.png)
- results of pressing `Ctrl+C` on items:
    ![Ctrl+C samples](screenshots/ctrlc-samples.png)


TODO / Intended Features
------------------------

- show custom info for each area (e.g. lab trials in area/connected area, skill point quest, etc.)
- UI for setting the configuration options in `config.json`, also more settings (e.g. enable/disable
    plugins)
- more info added to the reference
- rare item price estimation (AI-based)
- watching the trade river for expensive items to figure out "what's hot"
- ???


Usage & configuration
---------------------

- run `run.pyw` to start / `poehelper.exe` for the binary build (to see debug messages, run
    `rundbg.py` in a command line window)
- `Alt+F2` to minimize/maximize (minimizes itself when entering a game area)
- to get the lowest 20 prices for a unique item/currency/map/fragment/scarab, press 'Ctrl+C' on the
    item in-game (also to get a weapon's DPS)
- click on the buttons to open/close reference and plugin panels
- press `F` to jump to the search reference field
- to start/restart the timer click it
- to increment the counter click it, right click to reset to 0
- to move the window drag it (it will remember its position)
- on receiving a WTB PM, the position of the item is highlighted on a stash-like grid
- to change/disable macros, look for "MacroPlugin" in `data/config_plugins.json`
- to disable plugins, simply delete them
- `Alt+Q` to quit
- configuration: see `data/config.json` and `data/config_plugins.json`
    - `game.playername` should be set to use the self-kick macro


### Creating plugins

See the plugins in `data/plugins` for reference (no docs yet); the API functions exposed to the
plugins via `self.api` are in `poehelper/poeh_plugins.py` and the code is well-enough documented.


Requirements & Installation
---------------------------

- the game must be in "Windowed" or "Windowed Fullscreen" mode
- two "install" options exist:
    - via **git**:
        - make sure you have `Python 3` installed
        - `git clone --recursive https://gitlab.com/vtopan/poehelper`
        - `cd poehelper`
        - `pip3 install -r requirements.txt`
        - run `run.pyw` (e.g. `python3 run.pyw` or double-click)
    - via prebuilt executable:
        - **note**: this is usually a little behind the git version
        - go to <https://gitlab.com/vtopan/poehelper/tags/> and find the most recent release
        - download the corresponding zip (looks like `poehelper-<ver>-<gitcommit>.zip`)
        - extract the archive somewhere
        - run `poehelper/poehelper.exe` (`phconsole.exe` also shows a text console)
            - this creates the config file; close it, update the config & reopen it if needed


### Updating

As of version 0.3.7, an "Update" button exists (it retrieves updated files from gitlab if your
version is older than the gitlab version) - it's still experimental, so if it fails, simply delete
the old version and download the latest one (for the binary version, of course, the source version
can be `git pull`ed). Save `config.json` and `config_plugins.json` before deleting the folder in
case you've made changes you want to keep.


Feedback
--------

Feature requests and bug reports ideally via tickets on gitlab. Any other feedback: vtopan / gmail
or /u/vtopan on reddit.

