@echo off

set build=%~dp0build\poehelper
for /f %%i in ('git rev-parse --short HEAD') do set gitcommit=%%i

echo [*] Building in %build% (git@%gitcommit%)...
if not exist "%build%" md "%build%"

echo [*] Cleaning build folder...
rd /s /q %build%\poehelper
rd /s /q %build%\iolib
rd /s /q %build%\iopoelib
rd /s /q %build%\data
del %build%\version.txt
    
echo [*] Reading version...
for /f %%i in ('type version.txt') do set rlsver=%%i
if exist releases\poehelper-%rlsver%-%gitcommit%.zip del releases\poehelper-%rlsver%-%gitcommit%

echo [*] Build: %rlsver%. Copying the sources...
copy /Y version.txt %build%\version.txt
rem docs
md %build%\data %build%\data\plugins
xcopy /E data\doc %build%\data\doc\
xcopy /E data\rsrc %build%\data\rsrc\
copy data\plugins\*.py %build%\data\plugins >nul
rem poehelper
md %build%\poehelper
copy poehelper\*.py %build%\poehelper >nul
copy /Y CONTRIBUTING.md %build% >nul
copy /Y README.md %build% >nul
rem iolib
md %build%\iolib %build%\iolib\db %build%\iolib\net %build%\iolib\win
copy iolib\*.py %build%\iolib >nul
copy iolib\db\*.py %build%\iolib\db >nul
copy iolib\net\*.py %build%\iolib\net >nul
copy iolib\win\*.py %build%\iolib\win >nul
rem iopoelib
md %build%\iopoelib %build%\iopoelib\app %build%\iopoelib\misc %build%\iopoelib\net
copy iopoelib\*.py %build%\iopoelib >nul
copy iopoelib\app\*.py %build%\iopoelib\app >nul
copy iopoelib\misc\*.py %build%\iopoelib\misc >nul
copy iopoelib\net\*.py %build%\iopoelib\net >nul

echo [*] Cleaning up logs + config...
del %build%\poehelper.log
del %build%\pyloader.log
del %build%\data\config*.json
rd /s /q %build%\data\out

set archive=releases\poehelper-%rlsver%-%gitcommit%.zip
echo [*] Creating archive %archive%...
del releases\poehelper-%rlsver%-*.zip >nul
cd build & 7z a -r ..\%archive% poehelper & cd ..

echo [*] Done.
:done
rem pause if not interactive shell
echo %cmdcmdline%|find /i "%~0" >nul && pause
