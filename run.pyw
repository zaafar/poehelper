#!/usr/bin/env python3
"""
PoE pluginable overlay helper.

Author: Vlad Topan (vtopan/gmail)
"""

from poehelper.poehelper import PoEHelper


VER = '0.1.0 (dec.2018)'


PoEHelper().run()


