# Contributing to PoEHelper


## Help Wanted!

This being a game-related side project, there is no reasonable way in which I alone can take it as
far as it can go - help is most welcome. Whether improving/fixing/adding new reference pages (which
boils down to editing text files with a very simple, Markdown-like syntax) or fixing/improving the
code, any help is welcome.

Thanks to *Maximilian Brueckl* (maxbrueckl) for being the one to break the ice - it would have taken
me much longer to figure out this was an option without his pull request.


## Contributing Game Information

Simply edit any *.md file in `data\doc\reference` (or add a new one for relevant topics which aren't
present at all). Keep in mind this is an overlay help, so keep things:

- brief / concise:
    - both as ideas and as wording used - at most 6-8 words fit on a line, and Qt's QTextEdit
        doesn't count lines as they are displayed, word-wrapped, but as the number of newlines
        actually present, which makes sizing the window complicated if word wrapping becomes
        necessary
    - the space confinements are evident (the overlay can only take so much of the screen)
- clear:
    - some obvious abbreviations are okay, e.g. `exa` or `HoWA`, if they are either very common or
        easily identifiable with a web search
    - things like `MS` can be ambiguous - "movement speed" or "molten strike"
    - the target reader is not a complete PoE "newbie" (who would be very unlikely to have this
        program installed), but still someone who doesn't necessarily know and memorize in detail
        all the fine points of the game already (that profile wouldn't need this program at all)
- relevant:
    - the text should act as a quick reminder (see the target reader point above), not as an
        exhaustive reference or a guide - those would be hard to read in the confinement of the
        overlay window (a separate full, detailed reference can be written as regular Markdown and
        linked to from the reference panels via gitlab)
    - although everything related to the game could be relevant to someone at some point, given the
        limited space available, information of more general interest is preferred (e.g. the recipe
        for how to uncomplete a map vs. the best 6th link option for the insert-exotic-build-here)

Images can be used, and image symbols can help with brevity (e.g. using the `{prophecy}` symbol
instead of the actual word `prophecy`.

All the *.md files in `data\doc\reference` are (automatically) loaded by the reference plugin, so to
add a new reference page simply create a file there following the syntax below.


### Reference Syntax

- document/panel structure:
    - the page name is the first (and only) level one heading, which uses the `# Title Of The Page`
        variant from Markdown
    - the text after the title before the first second level (panel) title is the description
    - each panel is delimited by its second level heading, e.g. `## Panel Title`
    - keep document and panel titles short, they have to fit in tiny buttons (about 15 chars max)
- lines starting with `+`, `++` or `-` are highlighted (green = positive, as in "you want this", for
    `+` and `++` (the latter is also bold) and red = negative, as in "don't pick/do this" or "less
    useful" for `-`)
- `*italic*`, `**bold**` and `__underline__` from standard Markdown work
- see the `geninfo_plugin.py` for the implementation of the syntax (very rudimentary, regex-based)
- URLs starting with `http://` (or `https`) are automatically made clickable
- images can be inserted using curly braces
    - common item (currency, maps, etc.) images are downloaded automatically and the trade short
        names can be used (e.g. `{Exalted Orb}`, `{bloodstained-fossil}`, `{Polished Harbinger
        Scarab}`); see `data/rsrc/data-cache/poecom-trademap.json` for the full list
    - any image can be inserted by placing it in `data/rsrc/images` as a PNG and referring to it by
        the file name (with relative path to that folder if needed), e.g. `{prophecy}` will insert
        the prophecy symbol
- names in square brackets are converted to links to the PoE Wiki (e.g. `[Herald of Thunder]`)
- sample reference source excerpt:

    ~~~Markdown
    # Maps


    ## Academy (T6)

    - upgrade:
        - from [Maze], [Port]
        - to [Underground River]
    - {divcard} [A Dab of Ink]
    - items: Fingerless Silk Gloves


    ## Arcade (T7)

    - upgrade:
        - from [Phantasmagoria], [Temple]
    - {divcard} [The Saint's Treasure]
    - items: Two-Toned Boots
    ~~~

- see the reference pages in `data/doc/reference` for more examples


## Contributing Code

- code changes / additions can be submitted as patches via email or, preferably, as pull requests on
    gitlab
- existing style must be retained (more on this later), both in code and in text / titles
- commit titles should be explicit, start with a capital letter and briefly describe the change
- each commit must change a single conceptual thing (that may require several locations in one or
    more source files to be changed at the same time, which is ok as long as the alterations are
    part of the same, coherent semantic change)
    - this is important when trying to e.g. figure out which commit broke some minor feature two
        months ago and undo it - if the commits contain unrelated changes, this becomes very hard to
        do
- each pull request should tackle a single, coherent problem (usually this means one commit),
    otherwise changes that could go in immediately may be kept back by unrelated changes that need
    improving or are unacceptable for other reasons
- think of others when changing behavior:
    - don't assume everyone has the same setup and especially the same preferences / tastes
    - gate changes behind a default-off configuration setting if they have a reasonable impact


### Coding Style

- the existing coding style must be respected by all submitted code - even if your personal style
   choices are better, a poor but consistent coding style is much better that having patches of
   better and worse looking code
   - I can reformat patches but I can't reformat pull requests, so to avoid a long back and forth,
       please follow the existing coding style
- PEP8 is followed (the glaring exception is the 100 char per line), but other details are
    important as well (spaces around operators in most places, space only after a comma, lowercase
    with underscores identifier names ("snake_case"), etc.)


### Stuff that needs doing

- missing stuff:
    - proper documentation
    - configuration panel (UI which exposes `config.json` and `config_plugins.json`)
    - plugins:
        - plugin which tracks current area and gives advice during levelling (backed by an easy to
            edit Markdown-like text file with per-area entries)
        - DPS calculator plugin (based on weapon features, to make crafting easier)
    - reference pages:
        - best MS options / best MF options (regardless of build)
        - generic levelling uniques
        - gems (quality information and what vendor sells it, mostly)
- needs improving:
    - the plugin API was built by adding stuff when needed, needs more things exposed
    - reference pages: essences, vendors, making money
- todo (large items):
    - add plugin-accessible tiny indicators in compact title bar (in hidden mode)
    - rare item pricing AI
    - item filter quick editor? (e.g. "Highlight the last Ctrl+C-ed base/gem/map/etc.")
    - take a reference page, extract names (wiki links) and, where available, insert the poe.ninja
        price of the item
    - watch trade river in the background:
        - show expensive items which are actually selling
        - other uses?

If you want to work on any of the larger-sized items, please let me know ahead of time to avoid
redundant work.


## Thank you for all the feedback & help!

